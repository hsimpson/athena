Modules in this directory
-----

* [TrigPhotonFactories](TrigPhotonFactories.py)
  * ToolFactories to configure egammaAlgs to be used at the HLT
* [PhotonRecoSequences](PhotonRecoSequences.py)
  * Assembles the sequences for each stage of reconstruction
* [generatePhoton](generatePhoton.py)
  * Prototype for NewJO chain configuration